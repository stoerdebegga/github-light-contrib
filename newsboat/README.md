# Github Light Adaption for Newsboat

Adaption of [projekt0n](https://github.com/projekt0n)'s [Github Light](https://github.com/projekt0n/github-nvim-theme) for [Newsboat](https://newsboat.org/).

## Installation

1. Download the raw file:

```sh
curl https://codeberg.org/stoerdebegga/github-light-contrib/raw/branch/main/newsboat/github_light.colors \
--create-dirs -o ${HOME}/.config/newsboat/github_light.colors
```

2. Edit ${HOME}/.config/newsboat/config and add

```
include "~/.config/newsboat/github_light.colors"
```

## Screenshots

![NEWSBOAT01](.media/screenshot_001.png)
![NEWSBOAT02](.media/screenshot_002.png)
![NEWSBOAT03](.media/screenshot_003.png)

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT License, see LICENSE.
