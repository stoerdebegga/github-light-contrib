# Github Light Adaption for Powershell (Windows)

Adaption of [projekt0n](https://github.com/projekt0n)'s [Github Light](https://github.com/projekt0n/github-nvim-theme) for [Powershell](https://learn.microsoft.com/en-us/powershell/).

## Installation

1. Download the raw file:

```powershell
curl https://codeberg.org/stoerdebegga/github-light-contrib/raw/branch/main/powershell/GithubTheme/GithubTheme.psd1 \
--create-dirs -o "${HOME}\Downloads\testing\GithubTheme.psd1" \
https://codeberg.org/stoerdebegga/github-light-contrib/raw/branch/main/powershell/GithubTheme/GithubTheme.psm1 \
--create-dirs -o "${HOME}\Downloads\testing\GithubTheme.psm1"
```

2. Edit ${HOME}\Documents\PowerShell\Microsoft.Powershell_profile.ps1 and import the module

```
Import-Module GithubTheme

$Style = $GithubTheme['Light']
```

## Usage

Colors can be accessed like so 

```powershell
Write-Output "$($Style.Color4.Foreground()) Hello, world!"
```

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT License, see LICENSE.
