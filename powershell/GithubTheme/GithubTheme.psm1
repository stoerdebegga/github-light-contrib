class Colour
{
	<#
    .SYNOPSIS
        A class to represent a colour
    .DESCRIPTION
        A class to represent a colour, with methods to return the RGB values, HSL values, hex code, 
        and ANSI escape sequences for foreground and background colours
    #>

	# Required Values
	[byte]$Red
	[byte]$Green
	[byte]$Blue

	# Calculated Values
	hidden [double]$Hue
	hidden [double]$Saturation
	hidden [double]$Lightness

	# Flags
	hidden [bool]$HasHSL = $false

	Colour([byte]$Red, [byte]$Green, [byte]$Blue)
	{
		<#
        .SYNOPSIS
            Creates a new colour object
        .PARAMETER Red
            The red value of the colour
        .PARAMETER Green
            The green value of the colour
        .PARAMETER Blue
            The blue value of the colour
        #>
		$this.Red = $Red
		$this.Green = $Green
		$this.Blue = $Blue
	}

	[byte[]]RGB()
	{
		<#
        .SYNOPSIS
            Returns the seperate RGB values for the colour   
        #>
		return @($this.Red, $this.Green, $this.Blue)
	}

	[string]RGBString()
	{
		<#
        .SYNOPSIS
            Returns the RGB values for the colour as a string
        #>
		return "$($this.Red), $($this.Green), $($this.Blue)"
	}

	[double[]]HSL()
	{
		<#
        .SYNOPSIS
            Returns the seperate HSL values for the colour

        .DESCRIPTION
            Returns the seperate HSL values for the colour
            HSL values are returned as an array of doubles, in the order of Hue, Saturation, and Lightness
            Hue is returned in degrees, from 0 to 360
            Saturation and Lightness are returned as a value in the range of 0 to 1

        .NOTES
            This method is based on the algorithm found at:
            https://www.had2know.org/technology/hsl-rgb-color-converter.html
        #>

		# Return the values if they have already been calculated
		if ($this.HasHSL)
		{
			return @($this.Hue, $this.Saturation, $this.Lightness)
		}

		# Convert to a value from 0 to 1
		$local:R = $this.Red / 255
		$local:G = $this.Green / 255
		$local:B = $this.Blue / 255

		# Calculate Max and Min values - Nested because Math.Min/Max only take 2 arguments
		$local:Max = [Math]::Max([Math]::Max($R, $G), $B)
		$local:Min = [Math]::Min([Math]::Min($R, $G), $B) 

		# Calculate Lightness
		$local:L = ($Max + $Min) / 2

		# Calculate Saturation - if/else to avoid divide by 0
		$local:S = if ($L -gt 0)
		{ ($Max - $Min) / (1 - [Math]::Abs(2 * $L - 1)) 
		} else
		{ 0 
		}
        
		# Calculate Hue -> Convert to degrees - if/else to avoid NaN
		$local:H = if ($G -ne $B )
		{
			[Math]::Acos(($R - $G / 2 - $B / 2) / 
				[Math]::Sqrt([Math]::Pow($R, 2) + [Math]::Pow($G, 2) + [Math]::Pow($B, 2) -
					$R * $G - $R * $B - $G * $B)) * 180 / [Math]::PI
		} else
		{
			0
		}

		# Value above is only correct for H < 180, so we need to correct it for the blue half of the colour wheel
		if ($B -gt $G)
		{
			$H = 360 - $H 
		}

		# Set flag
		$this.HasHSL = $true

		# Cache values
		$this.Hue = $H
		$this.Saturation = $S
		$this.Lightness = $L

		return @($H, $S, $L)
	}

	[string]HSLString()
	{
		<#
        .SYNOPSIS
            Returns the HSL values for the colour as a formatted string
        
        .DESCRIPTION
            Returns the HSL values for the colour as a formatted string
            Hue is returned in degrees, from 0 to 360
            Saturation and Lightness are returned as a percentage, from 0 to 100
            All values are rounded to the nearest integer
        #>
		$local:HSL = $this.HSL()
		return "$([Math]::Round($HSL[0])), $([Math]::Round($HSL[1] * 100))%, $([Math]::Round($HSL[2] * 100))%"
	}

	[double[]]HSV()
	{
		<#
        .SYNOPSIS
            Returns the seperate HSV values for the colour
        
        .DESCRIPTION
            Returns the seperate HSV values for the colour
            HSV values are returned as an array of doubles, in the order of Hue, Saturation, and Value
            Hue is returned in degrees, from 0 to 360
            Saturation and Value are returned as a value in the range of 0 to 1
        #>

		# Derived from the HSL values
		$local:H, $local:S, $local:L = $this.HSL()

		# H = H
		$local:V = $L + $S * [Math]::Min($L, 1 - $L)
		$local:S = if ($V -ne 0)
		{ 2 * (1 - $L / $V) 
		} else
		{ 0 
		}

		return @($H, $S, $V)
	}

	[string]HSVString()
	{
		<#
        .SYNOPSIS
            Returns the HSV values for the colour as a formatted string
        
        .DESCRIPTION
            Returns the HSV values for the colour as a formatted string
            Hue is returned in degrees, from 0 to 360
            Saturation and Value are returned as a percentage, from 0 to 100
            All values are rounded to the nearest integer
        #>
		$local:HSV = $this.HSV()
		return "$([Math]::Round($HSV[0])), $([Math]::Round($HSV[1] * 100))%, $([Math]::Round($HSV[2] * 100))%"
	}

	[double[]]CMYK()
	{
		<#
        .SYNOPSIS
            Returns the CMYK values for the colour

        .DESCRIPTION
            Returns the CMYK values for the colour
            CMYK values are returned as value from 0 to 1
        #>
		$local:R = $this.Red / 255
		$local:G = $this.Green / 255
		$local:B = $this.Blue / 255

		$local:K = 1 - [Math]::Max([Math]::Max($R, $G), $B)
		$local:C = if ($K -ne 1)
		{ (1 - $R - $K) / (1 - $K) 
		} else
		{ 0 
		}
		$local:M = if ($K -ne 1)
		{ (1 - $G - $K) / (1 - $K) 
		} else
		{ 0 
		}
		$local:Y = if ($K -ne 1)
		{ (1 - $B - $K) / (1 - $K) 
		} else
		{ 0 
		}

		return @($C, $M, $Y, $K)
	}

	[string]Hex()
	{
		<#
        .SYNOPSIS
            Returns the hex code for the colour
        #>
		return "#$($this.Red.ToString('X2'))$($this.Green.ToString('X2'))$($this.Blue.ToString('X2'))"
	}

	[string]Foreground()
	{
		<#
        .SYNOPSIS
            Returns the ANSI Foreground escape sequence for the colour
        #>
		return "$([char]27)[38;2;$($this.Red);$($this.Green);$($this.Blue)m"
	}

	[string]Background()
	{
		<#
        .SYNOPSIS
            Returns the ANSI Background escape sequence for the colour
        #>
		return "$([char]27)[48;2;$($this.Red);$($this.Green);$($this.Blue)m"
	}

	[string]ToString()
	{
		<#
        .SYNOPSIS
            Returns the hex code for the colour
        #>
		return $this.Hex()
	}
}

class Style
{
	<#
    .SYNOPSIS
        A class to represent a style of GithubTheme
    #>
	[Colour]$Foreground
	[Colour]$Background
	[Colour]$Color0
	[Colour]$Color1
	[Colour]$Color2
	[Colour]$Color3
	[Colour]$Color4
	[Colour]$Color5
	[Colour]$Color6
	[Colour]$Color7
	[Colour]$Color8
	[Colour]$Color9
	[Colour]$Color10
	[Colour]$Color11
	[Colour]$Color12
	[Colour]$Color13
	[Colour]$Color14
	[Colour]$Color15
	[Colour]$Extra1
	[Colour]$Extra2
	[Colour]$Extra3
	[Colour]$Extra4

	[string]Blocks()
	{
		<#
        .SYNOPSIS
            Prints a block for each colour in the style
        #>
        
		return $($this.psobject.Properties | Select-Object -ExpandProperty Name | ForEach-Object {
				"$($this.$_.Background())   "
			}) -Join '' + "$([char]27)[0m"
	}

	Table()
	{
		<#
        .SYNOPSIS
            Prints a table of the colours in the style

        .DESCRIPTION
            Prints a table of the colours in the style, with the colour name, hex code, RGB values, and HSL 
            values. It also prints a circle of the colour so you can see what it looks like

            The output is intended to mimic the tables in the README.md of the Catppuccin/Catppuccin repo
        #>

		$this.psobject.Properties | Select-Object -ExpandProperty Name |
			Select-Object -Property `
			@{Name = ' '; Expression = { "$($this.$_.Foreground())$([char]0x2B24)$([char]27)[0m" } }, 
			@{Name = 'Name'; Expression = { $_.ToString() } }, 
			@{Name = 'Hex'; Expression = { $this.$_.Hex() } }, 
			@{Name = 'RGB'; Expression = { "rgb($($this.$_.RGBString()))" } }, 
			@{Name = 'HSL'; Expression = { "hsl($($this.$_.HSLString()))" } } | 
			Format-Table -AutoSize | Out-Host
	}
}

[Diagnostics.CodeAnalysis.SuppressMessageAttribute(
	'PSUseDeclaredVarsMoreThanAssignments', '',
	Justification = 'Variable is exported')]
$GithubTheme = @{
	'Light' = [Style]@{
		<# Github Theme: Light #>
		# Colors
		Foreground = [Colour]::new(255, 255, 255)
		Background = [Colour]::new(36, 41, 47)
		Color0     = [Colour]::new(36, 41, 46)
		Color1     = [Colour]::new(215, 58, 73)
		Color2     = [Colour]::new(40, 167, 69)
		Color3     = [Colour]::new(219, 171, 9)
		Color4     = [Colour]::new(3, 102, 214)
		Color5     = [Colour]::new(90, 50, 163)
		Color6     = [Colour]::new(5, 152, 188)
		Color7     = [Colour]::new(106, 115, 125)
		Color8     = [Colour]::new(149, 157, 165)
		Color9    = [Colour]::new(203, 36, 49)
		Color10    = [Colour]::new(34, 134, 58)
		Color11    = [Colour]::new(176, 136, 0)
		Color12    = [Colour]::new(0, 92, 197)
		Color13    = [Colour]::new(90, 50, 163)
		Color14    = [Colour]::new(49, 146, 170)
		Color15    = [Colour]::new(209, 213, 218)
		Extra1     = [Colour]::new(225, 228, 232)
		Extra2     = [Colour]::new(246, 248, 250)
		Extra3     = [Colour]::new(4, 66, 137)
		Extra4     = [Colour]::new(219, 233, 249)
	}
}


Export-ModuleMember -Variable GithubTheme
