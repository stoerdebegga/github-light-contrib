# Github Light Contributions

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

Adaptions of [projekt0n](https://github.com/projekt0n)'s [Github Light](https://github.com/projekt0n/github-nvim-theme) theme for several tools.

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT license, see LICENSE.
