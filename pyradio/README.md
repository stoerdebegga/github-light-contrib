# Github Light Adaption for PyRadio

Adaption of [projekt0n](https://github.com/projekt0n)'s [Github Light](https://github.com/projekt0n/github-nvim-theme) for [pyradio](https://www.coderholic.com/pyradio/).

## Installation

1. Download the raw file:

```sh
curl https://codeberg.org/stoerdebegga/github-light-contrib/raw/branch/main/pyradio/github_light.pyradio-theme \
--create-dirs -o ${HOME}/.config/pyradio/themes/github_light.pyradio-theme
```

2. Edit ${HOME}/.config/pyradio/config and add or replace existing theme

```
theme = github_light
```

## Screenshots

![PYRADIO01](.media/screenshot.png)

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT License, see LICENSE.
