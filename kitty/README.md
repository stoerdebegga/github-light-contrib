# Github Light Adaption for Kitty

Adaption of [projekt0n](https://github.com/projekt0n)'s [Github Light](https://github.com/projekt0n/github-nvim-theme) for [Kitty](https://github.comkovidgoyal/kitty).

## Installation

1. Download the raw file:

```sh
curl https://codeberg.org/stoerdebegga/github-light-contrib/raw/branch/main/kitty/github_light \
--create-dirs -o ${HOME}/.config/kitty/github_light.conf
```

2. Edit ${HOME}/.config/kitty/kitty.conf and add

```
include github_light.conf
```

## Screenshots

![KITTY001](.media/screenshot.png)

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT License, see LICENSE.
