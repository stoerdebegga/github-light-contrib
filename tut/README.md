# Github Light Adaption for tut

Adaption of [projekt0n](https://github.com/projekt0n)'s [Github Light](https://github.com/projekt0n/github-nvim-theme) for [tut](https://github.com/RasmusLindroth/tut) 

## Installation

1. Download the raw file:

```sh
curl https://codeberg.org/stoerdebegga/github-light-contrib/raw/branch/main/tut/github-light.toml \
--create-dirs -o ${HOME}/.config/tut/themes/github-light.toml
```

2. Edit ${HOME}/.config/tut/config.toml and replace exisiting colorscheme

```
theme="github_light"
```

## Screenshots

![TUT001](.media/screenshot.png)

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT License, see LICENSE.
