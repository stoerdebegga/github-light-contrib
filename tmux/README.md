# Github Light Adaption for tmux

Adaption of [projekt0n](https://github.com/projekt0n)'s [Github Light](https://github.com/projekt0n/github-nvim-theme) for [tmux](https://github.com/tmux/tmux/wiki).

## Installation

1. Download the raw file:

```sh
curl https://codeberg.org/stoerdebegga/github-light-contrib/raw/branch/main/tmux/github_light.tmux \
--create-dirs -o ${HOME}/.tmux/colors/github_light.tmux
```

2. Edit ${HOME}/.tmux.conf and add the following line

```
source-file $HOME/.tmux/colors/github_light.tmux
```

Afterwards start a new tmux session or reload the configuration by hitting <kbd>prefix</kbd> + <kbd>R<kbd>.

## Screenshots

![TMUX001](.media/screenshot.png)

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT License, see LICENSE.
