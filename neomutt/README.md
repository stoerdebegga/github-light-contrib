# Github  Light Adaption for Neomutt

Adaption of [projekt0n](https://github.com/projekt0n)'s [Github Light](https://github.com/projekt0n/github-nvim-theme) for [neomutt](https://neomutt.org/).

## Installation

1. Download the raw file:

```sh
curl https://codeberg.org/stoerdebegga/github-light-contrib/raw/branch/main/neomutt/github_light \
--create-dirs -o ${HOME}/.config/neomutt/github_light
```

2. Edit ${HOME}/.config/neomutt/neomuttrc and add

```
source ~/.config/neomutt/github_light
```

## Screenshots

![NEOMUTT001](.media/screenshot_001.png)
![NEOMUTT002](.media/screenshot_002.png)
![NEOMUTT003](.media/screenshot_003.png)

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT License, see LICENSE.
