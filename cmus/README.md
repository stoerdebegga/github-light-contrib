# Github Light Adaption for Cmus

Adaption of [projekt0n](https://github.com/projekt0n)'s [Github Light](https://github.com/projekt0n/github-nvim-theme) for [cmus](https://cmus.github.io/).

## Installation

1. Download the raw file:

```sh
curl https://codeberg.org/stoerdebegga/github-light-contrib/raw/branch/main/cmus/github_light.theme \
--create-dirs -o ${HOME}/.config/cmus/github_light.theme
```

2. Edit ${HOME}/.config/cmus/rc and add or replace exisiting colorscheme

```
colorscheme github_light
```

## Screenshots

![CMUS01](.media/screenshot001.png)
![CMUS02](.media/screenshot002.png)

## Author

stoerdebegga <https://codeberg.org/stoerdebegga>

## License

This software is released under the MIT License, see LICENSE.
